﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPass.aspx.cs" Inherits="Rane_WSD.ForgotPass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Forgot Password  | Rane TRW WSD</title>
    <link href="assets/css/icons.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
<%--       <style>
                .btn-primary{
            background-color:#09294e !important;
        }
        .topbar-header{
            background-color:#09294e !important;
        }
        .page-title-box {
            background-color:#09294e !important;
        }
        .side-navbar .metismenu>li>a.active {
    border-left: 4px solid #09294e;
    color: #09294e !important;
    background-color: #f7f7ff;
    font-weight:700;
}
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
            <header id="page-topbar" class="topbar-header">
         <div class="navbar-header">
            <div class="left-bar">
               <div class="navbar-brand-box">
                  <a href="index.html" class="logo logo-dark">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
                  <a href="index.html" class="logo logo-light">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
               </div>
              <%-- <button type="button" id="vertical-menu-btn" class="btn hamburg-icon">
                  <i class="mdi mdi-menu"></i>
               </button>--%>
              
                
            </div>
               <div class="text-center "><h3 class="text-light" >Warranty Complaints and Service Tracking(WSD)</h3> </div>
            
         </div>
      </header>
        <div class="auth-pages">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-11">
                        <div class="row justify-content-center">                            
                            <div class="col-md-6 pl-md-0">
                                <div class="card mb-0 p-md-3">
                                    <div class="card-body">
                                        <div class="clearfix text-center">
                                            <img src="assets/images/logo.jpg" height="75"  alt="logo" />
                                        </div>
                                         <h5 class="mt-4">Reset Password</h5>
              <p class="text-muted mb-4">Enter your email address and we'll send you an email with instructions to reset your password.</p>
             
                <div class="form-group floating-label">
                    <asp:TextBox ID="txtemail" TextMode="Email" class="form-control" runat="server"></asp:TextBox>
                  <label for="email">Email Address<span class="text-danger">*</span></label>
                  <div class="validation-error d-none font-size-13">
                    <p>Email must be a valid email address</p>
                  </div>
                </div>

                <div class="form-group text-center">
                    <asp:Button ID="txtforgot" class="btn btn-primary btn-block" runat="server" Text="Submit" />
                 
                </div>
              
              <div class="clearfix text-center">
                <p>Return to <a href="login.aspx" class="font-weight-bold text-primary">Login</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
  </div>
    
    </div>
            </div>
        
    </form>
</body>
</html>
