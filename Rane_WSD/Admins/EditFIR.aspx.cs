﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class EditFIR : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public bool noError = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                string editfir = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_FIRstatus", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editfir);
                cmd.Parameters.AddWithValue("@qtype", "updateaccount");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtFIRStatus.Text = sdr["FIRStatus"].ToString();
                    ddlStatus.SelectedValue = sdr["Status"].ToString();

                }
                con.Close();
            }
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            UpdateFIR();

            if (noError)
            {
                Response.Write("<script language='javascript'>alert('FIR Status Updated Successfully');window.location=('listFIR.aspx')</script>");
            }
            else
            {
                //pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }
        protected void  UpdateFIR()
        {

            string editfir = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());


            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_FIRstatus", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editfir);
                cmd.Parameters.AddWithValue("@qtype", "updateuser");
                cmd.Parameters.AddWithValue("@FIRStatus", txtFIRStatus.Text.Trim());
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@Updatedby", Session["UserName"].ToString());

                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();
                
            }
        }
    }
}