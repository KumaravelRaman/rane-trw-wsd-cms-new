﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="EditComplaint.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="Rane_WSD.Admins.EditComplaint" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Update FIR </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Update FIR </li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Update FIR  <button type="button" class="btn btn-info float-right btn"  data-toggle="modal" id="InfoAlert"
                                    data-target="#info-alert-modal">
                                    Info Alert</button>
 </h5>



                        </div>
                        <div class="card-body">






                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>




                            <div id="general2" class="custom-accordion" >

                                <div class="card shadow-none mb-3" style="display:none;">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true"
                                        aria-controls="collapseFour">
                                        <div class="card-header py-3" id="headingFour">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Note
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#general2">
                                        <div class="card-body">
                                            <p><b style="color: Grey">Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.</b></p>
                                        </div>
                                    </div>
                                </div>


                                <div class="card shadow-none mb-3">
                                    <a data-toggle="collapse" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                        <div class="card-header py-3" id="headingFive">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-information text-primary h5 mr-3"></i>FIR Details
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#general2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-4">

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>FIR No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblFirno" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtFirno" ForeColor="Red" ReadOnly="true" class="form-control" runat="server" placeholder="Enter FIRNo "></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtFirno" runat="server" ErrorMessage="Enter FIRNo"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Name </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblWSDName" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtWSDname" ReadOnly="true" class="form-control" runat="server" ForeColor="Blue" placeholder="Enter WSDName"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtWSDname" runat="server" ErrorMessage="Enter WSDName"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Branch </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblWSDBranch" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtWSDBranch" ReadOnly="true"  class="form-control" runat="server" ForeColor="Blue"  placeholder="Enter WSDBranch"></asp:TextBox>--%>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtWSDBranch" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Aggregate Serial No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblAggregateSerialNo" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtSerialno" ReadOnly="true" placeholder="Enter Aggregate Serial No" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtSerialno" ErrorMessage="Enter AggregateSerial No"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Upload File </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblUploadfile" style="display:none;" runat="server"></asp:Label>
                                                        <%-- <asp:FileUpload ID="FileUpload1" AllowMultiple="true" class="form-control" runat="server" />--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" ForeColor="Red" ControlToValidate="FileUpload1" runat="server" ErrorMessage="Choose File"></asp:RequiredFieldValidator>--%>
                                                        <%-- <asp:Label ID="listofuploadedfiles" runat="server" Text=""></asp:Label>--%>
                                                        <div class="row">
                                                            <asp:Panel ID="pnlPDF" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName %>"  data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF1" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName1 %>"  data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label3" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF2" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName2 %>" data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label4" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF3" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName3 %>" data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label5" Font-Bold="true" runat="server" Text=""></asp:Label>,
                                                                    <i class="fas fa-file-pdf"></i>,
                                                                </a>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-4">
                                                    <div class="form-group">

                                                        <label class="text-primary"><b>Retailer Name </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblretailername" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtRetailerName" ReadOnly="true" class="form-control" runat="server" placeholder="Enter Retailer Name"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" ControlToValidate="txtRetailerName" runat="server" ErrorMessage="Enter Retailer Name"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Retailer Location </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblretailerlocation" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtRetailerLocation"  ReadOnly="true" class="form-control" runat="server" placeholder="Enter Retailer Location"></asp:TextBox>--%>


                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="txtRetailerLocation" runat="server" ErrorMessage="Enter Retailer Location"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Invoice No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblInvoiceno" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtInvoiceNo" class="form-control" ReadOnly="true" runat="server" placeholder="Enter WSD Invoice No"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="Red" ControlToValidate="txtInvoiceNo" runat="server" ErrorMessage="Enter WSDInvoice No"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Date of Complaint </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblDateofComplaint" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtDateofComplaint" ReadOnly="true"  class="form-control" runat="server" TextMode="Date" placeholder="Enter Date "></asp:TextBox>--%>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" ForeColor="Red" runat="server" ControlToValidate="txtDateofComplaint" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Invoice Date</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblInvoiceDate" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtInvoicedate" ReadOnly="true" TextMode="Date" placeholder="Enter Invoice Date" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" runat="server" ControlToValidate="txtInvoicedate" ErrorMessage="Enter Invoice Date"></asp:RequiredFieldValidator>--%>
                                                    </div>



                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Part No</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblpartno" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtpartno" class="form-control" ReadOnly="true" runat="server" placeholder="Enter Part No"></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ControlToValidate="txtpartno" ErrorMessage="Enter Part No"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">

                                                        <label class="text-primary"><b>Part Type </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblparttype" runat="server"></asp:Label>
                                                        <%-- <asp:DropDownList ID="ddlParttype" AutoPostBack="true" OnSelectedIndexChanged="ddlParttype_SelectedIndexChanged" ReadOnly="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Aggregate">Aggregate</asp:ListItem>
                                            <asp:ListItem Value="Child Part">Child Part</asp:ListItem>
                                        </asp:DropDownList>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="ReqParttype" ForeColor="Red" ControlToValidate="ddlParttype" runat="server" ErrorMessage="Choose PartType"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Quantity</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtQuantity" ReadOnly="true"  class="form-control" runat="server" placeholder="Enter Quantity"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ForeColor="Red" ControlToValidate="txtQuantity" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Status :</b></label>
                                                        <asp:Label ID="lblStatus" CssClass="text-info" Font-Bold="true"  runat="server"></asp:Label>
                                                        <%-- <asp:DropDownList ID="ddlStatus" ReadOnly="true" class="form-control" runat="server">
                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                        </asp:DropDownList>--%>
                                                    </div>


                                                </div>
                                                <div class="pl-3">
                                                    <%-- <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Update" />--%>
                                                    <%--  <a href="ListComplaint.aspx" class=" btn btn-dark btn-sm" >Back</a>--%>
                                                </div>
                                                <!-- end col -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- collapse one end -->


                                  <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="true"
                                        aria-controls="collapseSix">
                                        <div class="card-header py-3" id="headingSix">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-refresh-circle text-primary h5 mr-3"></i>Update FIR
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#general2">
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-lg-4">
                                                    <asp:Panel ID="pnlStatus" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Status </b><span class="text-danger">*</span></label>

                                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="ddlStatus" runat="server" ErrorMessage="Choose Status"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <div class="form-group">
                                                        <asp:Label ID="lblRemarks" Font-Bold="true" runat="server" Text="Remarks"><span class="text-danger">*</span></asp:Label>

                                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" placeholder="Enter Remarks" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtRemarks" ErrorMessage="Enter Remarks"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                               
                                              </div>
                                                <div class="col-lg-6">
                                                    <div class="text-center">
                                                        <asp:Button runat="server" CssClass="btn btn-primary " ID="btnSubmit" OnClick="btnSubmit_Click" Text="Update" />
                                                        <a href="ListComplaint.aspx" class=" btn btn-dark ">Back</a>
                                                    </div>

                                                </div>

                                            
                                        </div>
                                    </div>
                                </div>
                                

                                <div id="info-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="text-center">
                                                    <i class="bx bx-info-circle fs-xl text-info"></i>
                                                    <h4 class="mt-2">Notes !</h4>
                                                    <p class="mt-3">
                                                        Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.
                                                    </p>
                                                    <button type="button" id="btnDismiss" class="btn btn-info my-2" data-dismiss="modal">Continue</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- collapse two end -->



                            </div>
                            <!-- end #accordions-->
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
</asp:Content>
