﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class ViewComplaint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();
        public bool noError = true;
        public bool IsUpdated;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindValue();
                dataget();

            }
        }
        //protected void BindValue()
        //{
        //    SqlConnection con = new SqlConnection(cs);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("Select distinct FIRStatus,Id from [tbl_FIRstatus] where FIRStatus !=''", con);
        //    //string com = "Select distinct [Id],FIRStatus from [tbl_FIRstatus] where Id in (2,3)";
        //    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    adpt.Fill(dt);
        //    DropDownList1.DataSource = dt;
        //    DropDownList1.DataTextField = "FIRStatus";
        //    DropDownList1.DataValueField = "Id";
        //    DropDownList1.DataBind();
        //    DropDownList1.Items.Insert(0, new ListItem("-- Select FIRStatus --", ""));
        //}
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);

            SqlCommand cmd = new SqlCommand("sp_b_AdminApproved", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();


            cmd.Parameters.AddWithValue("@qtype", "selectadmin");

            cmd.Parameters.AddWithValue("@FIRNo", txtFIRNO.Text.Trim());
            //cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());
            cmd.Parameters.AddWithValue("@WSDName", txtWSDName.Text.Trim());
            cmd.Parameters.AddWithValue("@Status", txtWSDBranch.Text.Trim());
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            ListView1.DataSource = ds;

            ListView1.DataBind();
            con.Close();
        }
        public string Id;
        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                Id = e.CommandArgument.ToString();

                deleteFn(Id);
            }
            if (noError)
            {
                Response.Redirect("ListComplaint.aspx");
            }
            else
            {

                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }

        }

        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {




        }
        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("dataPagerNumeric") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.dataget(); ;
        }
        protected string deleteFn(string Id)
        {
            SqlConnection con = new SqlConnection(cs);
            string rStatus = "";
            try
            {

                SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@qtype", "deleteFIR");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();

            }


            return rStatus;
        }
        protected void ListView1_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            dataget();
        }
    }
}