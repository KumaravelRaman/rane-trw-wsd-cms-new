﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="AccountCreation.aspx.cs" Inherits="Rane_WSD.AccountCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- page header -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">User Account Creation</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Account Creation</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"></h5>

                        </div>
                        <div class="card-body">
                             <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label>Name <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtusername" class="form-control" runat="server" placeholder="Enter Name "></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtusername" runat="server" ErrorMessage="Enter Username"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Email ID <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtemail" class="form-control" runat="server" placeholder="Enter Email"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtemail" runat="server" ErrorMessage="Enter Email"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group">
                                        <label>Mobile No <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtmobile" MaxLength="10" MinLength="10"  class="form-control" runat="server" placeholder="Enter Mobile No"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtmobile" runat="server" ErrorMessage="Enter Mobile"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <asp:DropDownList ID="ddlStatus" class="form-control" runat="server">
                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">
                                     <div class="form-group">
                                        <label>Branch Name <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtBranchName" class="form-control" runat="server" placeholder="Enter Branch"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" ControlToValidate="txtBranchName" runat="server" ErrorMessage="Enter Branch"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>State <span class="text-danger">*</span></label>

                                        <asp:DropDownList ID="ddlstate" class="form-control" runat="server" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="ddlstate" runat="server" ErrorMessage="Choose State"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>City <span class="text-danger">*</span></label>
                                         <asp:DropDownList ID="ddlcity" class="form-control" runat="server">
                                             <asp:ListItem>Select City</asp:ListItem>
                                         </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ControlToValidate="ddlcity" ErrorMessage="Enter City"></asp:RequiredFieldValidator>
                                    </div>
                                    
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <div class="form-group">

                                        <label>UserType <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtUserType" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                        <asp:DropDownList ID="ddlUsertype" disabled="true" CssClass="form-control" style="display:none;"  runat="server"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="ddlUsertype" runat="server" ErrorMessage="Choose Usertype"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label>Code<span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtcode" class="form-control" runat="server" placeholder="Enter Code"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="Red" ControlToValidate="txtcode" runat="server" ErrorMessage="Enter Code"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label> Location <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtlocation" TextMode="MultiLine" placeholder="Enter Location" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtlocation" ErrorMessage="Enter Location"></asp:RequiredFieldValidator>
                                    </div>

                                    
                                </div>
                                <div class="pl-3">
                                    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Submit" />
                                      <a href="listaccount.aspx?usertype=<%Response.Write(usertype); %>" class=" btn btn-dark " >Back</a>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
