﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="EditFIR.aspx.cs" Inherits="Rane_WSD.EditFIR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Edit FIR</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Edit FIR</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-10">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"></h5>

                        </div>
                        <div class="card-body">
                             <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            <div class="row">
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <label>FIRStatus <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtFIRStatus" class="form-control" runat="server" placeholder="Enter FIR Status "></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtFIRStatus" runat="server" ErrorMessage="Enter FIR Status"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    
                                </div>
                                <!-- end col -->

                               
                              
                                <!-- end col -->

                                <div class="col-lg-6">

                                  
                                    <div class="form-group">
                                        <label>Status</label>
                                        <asp:DropDownList ID="ddlStatus" class="form-control" runat="server">
                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="pl-3">
                                    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Update" />
                                      <a href="listFIR.aspx" class=" btn btn-dark " >Back</a>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
</asp:Content>
