﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="listServiceEngineer.aspx.cs" Inherits="Rane_WSD.Admins.listServiceEngineer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   <%-- <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $("#mytable tr").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">List of Service Engineer</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">List of Service Engineer</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer List</h5>
                            <h6 class="card-subtitle"></h6>
                        </div>
                        <div class="card-body">
                            <div class="col-md-4">
                             
                                 

                            </div>
                            <div class="card">
                    <div class="row">
                         <div class="btnsprimary col-md-12 pull-left">
                             <div class="row">
                        <div class="col-md-3 pull-left">
                            <asp:TextBox runat="server" ID="txtName" placeholder="Search By Name..." CssClass="form-control"></asp:TextBox>
                            </div>
                         <div class="col-md-3 pull-left">
                            <asp:TextBox runat="server" ID="txtsecode" placeholder="Search by SECode ..." CssClass="form-control"></asp:TextBox>
                            </div>
                         <div class="col-md-3 pull-left">
                            <asp:TextBox runat="server" ID="txtstate" placeholder="Search by State ..." CssClass="form-control"></asp:TextBox>
                            </div>
                         
                        <div class="col-md-3 pull-left">
                            <asp:Button class="btn btn-primary" ID="Button1" ValidationGroup="btnSearch" Text="Search" runat="server" OnClick="btn_submit_Click" />


                        </div>
                                 </div>
                             </div>
                           <%-- <div class="col-md-1 offset-md-9">
                                Search :
                            </div>
                            <div class="col-md-3 pb-2 float-right">
                                <input class="form-control" id="myInput" type="text" placeholder="Type Something..." />
                            </div>--%>
                            <div class="table-responsive">
                                <asp:ListView ID="ListView1" runat="server" DataKeyNames="SEID" ItemPlaceholderID="itmPlaceholder" OnPagePropertiesChanging="OnPagePropertiesChanging" ConvertEmptyStringToNull="true" OnItemDeleting="ListView1_ItemDeleting" OnItemCommand="ListView1_ItemCommand">
                                    <LayoutTemplate>
                                        <table
                                            class="table table-bordered ">
                                            <thead class="thead-light">
                                                <tr>
                                                   <th class="text-center">S.No</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Address</th>
                                                    <th class="text-center">SE Code</th>                                                                                                <th class="text-center">ContactNo/MobileNo</th>
                                                    
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center">State</th>
                                                    <th class="text-center" style="width:50px;">Action</th>
                                                </tr>
                                                <tr>
                                                    <asp:PlaceHolder  id="itmPlaceholder" runat="server"></asp:PlaceHolder>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="6">
                                                    <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                            <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                            <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                                <td colspan="6" class="number_of_record" style="text-align: right">
                                                    <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:TemplatePagerField>
                                                                <PagerTemplate>
                                                                    <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                                    </span>
                                                                </PagerTemplate>
                                                            </asp:TemplatePagerField>
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                            </tr>

                                        </table>

                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tbody id="mytable">
                                            <tr>
                                                <td class="text-center">
                                                    <asp:Label ID="Label1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem,"slNo")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Name")%>' />
                                                   
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("Address")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("secode")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("contactNo")%>'></asp:Label>/<%#Eval("mobileNo")%>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("emailId")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("State")%>'></asp:Label>
                                                </td>
                                               
                                                <td class="text-center">
                                                   <%-- <a href="EditAccount.aspx?ASCID=<%# Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"ASCID").ToString()) %>" title="Edit User" data-toggle="tooltip" data-placement="top" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>--%>

                                                   <%-- <asp:LinkButton ID="lnkDelete" ToolTip="Delete User" CommandName="delete" CommandArgument='<%# Eval("Id") %>' class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete User' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></asp:LinkButton>--%>
                                                    <a href="ViewServiceEng.aspx?SEID=<%#  Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"SEID").ToString()) %>" title="View ServiceEng" data-toggle="tooltip" data-placement="top" class="btn btn-secondary btn-sm "><i class="fa fa-eye "></i></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div class="widget-content">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                             <th class="text-center">S.No</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Address</th>
                                                    <th class="text-center">SE Code</th>                                                                                                <th class="text-center">ContactNo/MobileNo</th>
                                                    
                                                    <th class="text-center">Email</th>
                                                    <th class="text-center">State</th>
                                                    <th class="text-center" style="width:50px;">Action</th>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <td colspan="7" style="background-color: white; color: black;">No Records Found</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:ListView>

                            </div>
                        </div>
                        <!-- end card-box-->
                    </div>
                    <!-- end col-->
                </div>
            </div>
        </div>
        </div>
        <!-- end row -->
</asp:Content>
