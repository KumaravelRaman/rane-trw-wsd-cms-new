﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class listaccount : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();
        public string usertype = "";
        public bool noError = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            usertype = Request.QueryString["usertype"].ToString();
            if (!IsPostBack)
            {
                dataget();
            }
        }
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_user", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@qtype", "selectlist");
            cmd.Parameters.AddWithValue("@EmailId", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@Mobile", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@UserType", usertype);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            ListView1.DataSource = ds;
            ListView1.DataBind();
            con.Close();
        }
        public string Id;
        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                Id = e.CommandArgument.ToString();
                deleteFn(Id);
                if (noError)
                {
                    Response.Redirect("listaccount.aspx?usertype=" + usertype);
                }
                else
                {

                    lblError.Text = "Error Occured please Contact Administrator";
                    noError = false;
                }
            }
        }

        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            
        }
        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("dataPagerNumeric") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.dataget(); ;
        }
        protected string deleteFn(string Id)
        {
            SqlConnection con = new SqlConnection(cs);
            string rStatus = "";
            try
            {

                SqlCommand cmd = new SqlCommand("sp_b_user", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@qtype", "deleteuser");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();               
               
            }

            return rStatus;
        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            dataget();
        }
    }
}