﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="ViewFIR.aspx.cs" Inherits="Rane_WSD.ViewFIR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">View FIRStatus </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">View FIRStatus </li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-10">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"></h5>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <label class="text-primary"><b>FIRStatus </b><span class="text-danger">:</span></label>
                                  <asp:Label ID="lblFIRStatus" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtusername" runat="server" ErrorMessage="Enter Username"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>CreatedBy</b> <span class="text-danger">:</span></label>
                                       <asp:Label ID="lblcreatedby" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtemail" runat="server" ErrorMessage="Enter Email"></asp:RequiredFieldValidator>--%>
                                    </div>

                                   
                                </div>
                                <!-- end col -->

                               
                               
                                <!-- end col -->

                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <label class="text-primary"><b>UpdatedBy </b><span class="text-danger">:</span></label>
                                      <asp:Label ID="lblupdateby" runat="server" Text=""></asp:Label>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="ddlUsertype" runat="server" ErrorMessage="Choose Usertype"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Status :</b></label>
                                       <asp:Label ID="lblstatus" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="pl-3">
                                    <%--<asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Submit" />--%>
                                    <a href="listFIR.aspx" class=" btn btn-dark btn-sm" >Back</a>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
