﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Rane_WSD.ChangePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Change Password  | Rane TRW WSD</title>
    <link href="assets/css/icons.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <header id="page-topbar" class="topbar-header">
         <div class="navbar-header">
            <div class="left-bar">
               <div class="navbar-brand-box">
                  <a href="index.html" class="logo logo-dark">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
                  <a href="index.html" class="logo logo-light">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
               </div>
              <%-- <button type="button" id="vertical-menu-btn" class="btn hamburg-icon">
                  <i class="mdi mdi-menu"></i>
               </button>--%>
              
                
            </div>
               <div class="text-center "><h3 class="text-light" >Warranty Complaints and Service Tracking</h3> </div>
            
         </div>
      </header>
        <div class="auth-pages">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-11">
                        <div class="row justify-content-center">                            
                            <div class="col-md-6 pl-md-0">
                                <div class="card mb-0 p-md-3">
                                    <div class="card-body">
                                        <div class="clearfix text-center">
                                           <%-- <img src="assets/images/logo.jpg" height="75"  alt="logo" />--%>
                                        </div>
                                         <h5 class="mt-4" style="text-align:center;font-weight: 900;
    font-size: 20px;">Change Password</h5>
             
                <div class="form-group">
                                                    <label for="emailaddress">Old Password<span style="color: red;">*</span></label>
     <asp:TextBox ID="txtoldPassword" placeholder="Enter Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>    
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtoldPassword"  Display="Dynamic" ErrorMessage="Enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lblOldPassword" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                </div>
                                             <div class="form-group">
                                                    <label for="emailaddress">New Password<span style="color: red;">*</span></label>
     <asp:TextBox ID="TextBox1" placeholder="Enter Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>    
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"  Display="Dynamic" ErrorMessage="Enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                             <div class="form-group">
                                                    <label for="emailaddress">Confirm Password<span style="color: red;">*</span></label>
     <asp:TextBox ID="TextBox2" placeholder="Enter Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>    
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox2"  Display="Dynamic" ErrorMessage="Enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                                                 <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                </div>
                                            <%-- <div class="form-group mb-4 pb-3">
                                                    <div class="custom-control custom-checkbox checkbox-primary">
                                                     <a href="Login.aspx" class="text-muted float-right">Back to Login</a>   
                                                    </div>
                                                </div>--%>
                                            <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>

                <div class="form-group text-center">
                   <asp:Button ID="btnForgot" runat="server" class="btn btn-primary btn-block"  Text="Reset Password" OnClick="btnForgot_Click" />
                 
                </div>
              
              <div class="clearfix text-center">
                <p>Return to <a href="login.aspx" class="font-weight-bold text-primary">Login</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
  </div>
    
    </div>
            </div>
        
    </form>
</body>
</html>
