﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="ViewComplaint.aspx.cs" Inherits="Rane_WSD.User.ViewComplaint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">View Complaint </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">View Complaint </li>
                </ol>
            </div>
        </div>
    </div>

   
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">



                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">View FIR Details <a href="ListComplaint.aspx" class=" btn btn-dark float-right btn-sm">Back</a></h5>
                            <div class="pl-12">

                                <%--<a href="ListComplaint.aspx" class=" btn btn-dark btn-sm">Back</a>--%>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="general7" class="custom-accordion">

                                        <div class="card shadow-none mb-3">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseNine" aria-expanded="true"
                                                aria-controls="collapseNine">
                                                <div class="card-header py-3" id="headingNine">
                                                    <h6 class="mb-0">
                                                        <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>FIR Details
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                    </h6>
                                                </div>
                                            </a>

                                            <div id="collapseNine" class="collapse show" aria-labelledby="headingNine" data-parent="#general7">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-4">

                                                            <div class="form-group">
                                                                <label class="text-primary"><b>FIR NO </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblFirno" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>WSD Name</b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblWSDname" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="text-primary"><b>WSDBranch</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblWSDbranch" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Aggregate Serial No </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblAggregateSerialNo" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b> Upload File </b><span class="text-danger">:</span></label>
                                                               <asp:Label ID="lblUploadFile" Font-Bold="true" style="display:none;" runat="server" Text=""></asp:Label>
                                                                 <div class="row">
                                                                <asp:Panel ID="Panel1" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName4 %>"  data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                         <asp:Label ID="Label4" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                      <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel2" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName5 %>"    data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label5" Font-Bold="true" runat="server" Text=""></asp:Label>    <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel3" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName6 %>"   data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                        ,<asp:Label ID="Label6" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel4" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName7 %>"   data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                        ,<asp:Label ID="Label7" Font-Bold="true" runat="server" Text=""></asp:Label>,
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                            </div>

                                                            </div>

                                                        </div>
                                                        <!-- end col -->

                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Retailer Name </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblRetailername" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Retailer Location </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblRetailerLocation" Font-Bold="true" runat="server" Text=""></asp:Label>


                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>WSD Invoice No </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblInvoiceNo" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Date of Complaint </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblDateofComplaint" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Part No </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblPartno" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>

                                                        </div>
                                                        <!-- end col -->

                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Invoice Date </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblInvoiceDate" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>PartType</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblPartType" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Quantity</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblQuantity" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Notes </b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblNotes" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Status</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblStatus" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            </div>





                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                         <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlRejected" runat="server" Visible="false">
                                        <div>
                                            <div id="general20" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwenty" aria-expanded="true"
                                        aria-controls="collapseTwenty">
                                        <div class="card-header py-3" id="headingTwenty">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Sales Rejected Reason
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseTwenty" class="collapse show" aria-labelledby="headingTwenty" data-parent="#general20">
                                        <div class="card-body">
                                            <div class="form-group">
                                                            <label class="text-primary"><b>Reason For Rejection </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label16" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label17" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label18" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                 <div class="col-lg-6">
                                <asp:Panel ID="pnlApproved" runat="server" Visible="false">
                                   
                                        <div id="general2" class="custom-accordion">
                                            <div class="card shadow-none mb-3">
                                                <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    <div class="card-header py-3" id="headingFour">
                                                        <h6 class="mb-0">
                                                            <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Approved / Reject Sales Engineer
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                        </h6>
                                                    </div>
                                                </a>

                                                <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#general2">
                                                    <div class="card-body">
                                                        <div>
                                                            <div class="form-group">

                                                                <label class="text-primary"><b>Allocated Service Eng </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblAllocateServiceEng" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">

                                                                <label class="text-primary"><b>Remarks </b><span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblRemarks" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group" style="display:none;">
                                                                <label class="text-primary"><b>Reason For Rejection</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblReasonForRejection" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">

                                                                <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblUpdatedOn" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>

                                                            <div class="form-group">

                                                                <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblUpdatedBy" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                </asp:Panel>
                                     </div>
                                 <div class="col-lg-6">
                                <asp:Panel ID="pnlAllocateASC" runat="server" Visible="false">
                                  
                                        <div id="general3" class="custom-accordion">
                                            <div class="card shadow-none mb-3">
                                                <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="true"
                                                    aria-controls="collapseFive">
                                                    <div class="card-header py-3" id="headingFive">
                                                        <h6 class="mb-0">
                                                            <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Allocated To ASC
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                        </h6>
                                                    </div>
                                                </a>

                                                <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#general3">
                                                    <div class="card-body">
                                                        <div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Allocated ASC</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblAllocateASC" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblupda" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblupdOn" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Remarks</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblremark1" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                               </asp:Panel>
                                      </div>
                            </div>
                        </div>


                        <div class="card-body">
                            <div class="row">
                                 <div class="col-lg-6">
                                <asp:Panel ID="pnlDispatchASC" runat="server" Visible="false">
                                   
                                        <div id="general4" class="custom-accordion">

                                            <div class="card shadow-none mb-3">
                                                <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="true"
                                                    aria-controls="collapseSix">
                                                    <div class="card-header py-3" id="headingSix">
                                                        <h6 class="mb-0">
                                                            <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Material Dispatched To ASC
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                        </h6>
                                                    </div>
                                                </a>

                                                <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#general4">
                                                    <div class="card-body">
                                                        <div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Allocated ASC</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblAlloASC" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Dispatch Method</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblDispatchMethod" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Document No</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblDocumentNo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Dispatch Date</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblDispatchdate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Remarks</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblrem" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblupd" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                                <asp:Label ID="lblupdby" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                  

                                </asp:Panel>
                                       </div>
                                 <div class="col-lg-6">
                                 <asp:Panel ID="pnlReceivedWSD" runat="server" Visible="false"> 
                              
                                    <div id="general5" class="custom-accordion">

                                        <div class="card shadow-none mb-3">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseSeven" aria-expanded="true"
                                                aria-controls="collapseSeven">
                                                <div class="card-header py-3" id="headingSeven">
                                                    <h6 class="mb-0">
                                                        <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>ASC Observation/Material Received From WSD
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                    </h6>
                                                </div>
                                            </a>

                                            <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#general5">
                                                <div class="card-body">
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>ASC Recommendation</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblRecommendation" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>ASC Reported Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblASCReporteddate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Observation of Problem</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblObservationofProblem" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Received Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblReceivedDate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Uploaded File</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblUploadedFiles" Font-Bold="true" runat="server" style="display:none;"  Text=""></asp:Label>
                                                            <div class="row">
                                                                <asp:Panel ID="pnlPDF" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName %>" style="color:#d34372"   data-toggle="tooltip" data-placement="top">
                                                                         <asp:Label ID="Label8" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlPDF1" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName1 %>"  style="color:#d34372" data-toggle="tooltip" data-placement="top">
                                                                         ,<asp:Label ID="Label9" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlPDF2" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName2 %>" style="color:#d34372"  data-toggle="tooltip" data-placement="top">
                                                                         ,<asp:Label ID="Label10" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlPDF3" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName3 %>" style="color:#d34372"  data-toggle="tooltip" data-placement="top">
                                                                         ,<asp:Label ID="Label11" Font-Bold="true" runat="server" Text=""></asp:Label>,
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdat" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdatOn" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              
                                </asp:Panel>
                                        </div>

                            </div>
                        </div>


                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlServiceEngApprove" runat="server" Visible="false">
                                        <div id="general9" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        <div class="card-header py-3" id="headingOne">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Service Eng Approve / Reject
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#general9">
                                        <div class="card-body">
                                         <div class="form-group">
                                               <label class="text-primary"><b>Service Eng Recommendation</b> <span class="text-danger">:</span></label>
                                             <asp:Label ID="lblServiceRecommended" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                                           </div>
                                            <div class="form-group">
                                               <label class="text-primary"><b>Remarks</b> <span class="text-danger">:</span></label>
                                             <asp:Label ID="lblremarks3" runat="server" Font-Bold="true" Text=""></asp:Label>
                                           </div>
                                            <div class="form-group">
                                               <label class="text-primary"><b>Updated On </b> <span class="text-danger">:</span></label>
                                             <asp:Label ID="UpdatedOn" runat="server" Font-Bold="true" Text=""></asp:Label>
                                           </div>
                                            <div class="form-group">
                                               <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                             <asp:Label ID="UpdatedBy" runat="server" Font-Bold="true" Text=""></asp:Label>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                                <div class="col-lg-6">
                                <asp:Panel ID="pnlDispatchWSD" runat="server" Visible="false"> 

                                    <div id="general6" class="custom-accordion">

                                        <div class="card shadow-none mb-3">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseEight" aria-expanded="true"
                                                aria-controls="collapseEight">
                                                <div class="card-header py-3" id="headingEight">
                                                    <h6 class="mb-0">
                                                        <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Material Dispatched To WSD
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                    </h6>
                                                </div>
                                            </a>

                                            <div id="collapseEight" class="collapse show" aria-labelledby="headingEight" data-parent="#general6">
                                                <div class="card-body">
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>LR No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lbllrno" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>LR Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblLrdate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>DC No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblDcNo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Remarks</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblre" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdatedby1" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated on</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdatedOn1" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </asp:Panel>
                          </div>
                                
                            </div>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlReceiApprove" runat="server" Visible="false">

                                       <div id="general11" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="true"
                                        aria-controls="collapseThree">
                                        <div class="card-header py-3" id="Div1">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Material Received From ASC
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseThree" class="collapse show" aria-labelledby="headingNine" data-parent="#general11">
                                        <div class="card-body">
                                             <div class="form-group">
                                                            <label class="text-primary"><b>Uploaded File</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupload20" style="display:none;" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                            <div class="row">
                                                                <asp:Panel ID="Panel5" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName9 %>" style="color:#d34372" data-toggle="tooltip" data-placement="top">
                                                                        <asp:Label ID="Label12" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel6" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName10 %>" style="color:#d34372"  data-toggle="tooltip" data-placement="top">
                                                                        ,<asp:Label ID="Label13" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel7" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName11 %>" style="color:#d34372" data-toggle="tooltip" data-placement="top">
                                                                        ,<asp:Label ID="Label14" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                                <asp:Panel ID="Panel8" runat="server">
                                                                    <a target="_blank" href="../Upload/<%= pdfName12 %>" style="color:#d34372"  data-toggle="tooltip" data-placement="top">
                                                                        ,<asp:Label ID="Label15" Font-Bold="true" runat="server" Text=""></asp:Label>,
                                                                        <i class="fas fa-file-pdf"></i>
                                                                    </a>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>

                                             <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated20" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdate21" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>

                                    </asp:Panel>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlSendLRNo" runat="server" Visible="false">
                                        <div id="general12" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwelve" aria-expanded="true"
                                        aria-controls="collapseTwelve">
                                        <div class="card-header py-3" id="headingTwelve">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Material Dispatched To Warehouse
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseTwelve" class="collapse show" aria-labelledby="headingTwelve" data-parent="#general12">

                                        <div class="card-body">
                                            <div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>LR No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblLRNoteno" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>LR Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblLRNoteDate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Delivery Note No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblDeliveryNote" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Transportation Name</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblTransportation" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Remarks </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblreman" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated13" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated14" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlReceiveWarehouse" runat="server" Visible="false">

                                        <div id="general13" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThirteen" aria-expanded="true"
                                        aria-controls="collapseThirteen">
                                        <div class="card-header py-3" id="headingThirteen">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Material Receieved From WSD / Warehouse
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseThirteen" class="collapse show" aria-labelledby="headingThirteen" data-parent="#general13">
                                        <div class="card-body">
                                           <div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Inward Number</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblInwardNumber" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Inward Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblInwardDate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Quantity</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblQuantity20" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Remarks</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblRemarks20" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdate15" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdate16" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlCreditNoteUpdation" runat="server" Visible="false">
                                        <div id="general14" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFourteen" aria-expanded="true"
                                        aria-controls="collapseFourteen">
                                        <div class="card-header py-3" id="headingFourteen">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Credit Note Updation
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFourteen" class="collapse show" aria-labelledby="headingFourteen" data-parent="#general14">
                                        <div class="card-body">
                                            <div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Credit Note No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblCreditNoteNo" ForeColor="Red" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Credit Note Date</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblCreditNotedate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Value</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblValue" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Plant Document No</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblPlantDocumentNo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Plant Document Date </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblPlantDocumentDate" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Plant Notification No </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblPlantNotifyNo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Remarks </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblremaks25" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated18" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated19" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlRejectedASCFeedBack" runat="server" Visible="false">
                                        <div id="general16" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseSixteen" aria-expanded="true"
                                        aria-controls="collapseSixteen">
                                        <div class="card-header py-3" id="headingSixteen">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Service Eng Rejected ASC FeedBack
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseSixteen" class="collapse show" aria-labelledby="headingSixteen" data-parent="#general16">
                                        <div class="card-body">
                                             <div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Remarks </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblremarks26" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated27" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblupdated28" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlSendRetailer" runat="server" Visible="false">
                                        <div id="general17" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseSeventeen" aria-expanded="true"
                                        aria-controls="collapseSeventeen">
                                        <div class="card-header py-3" id="headingSeventeen">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>WSD Material SendBack To Retailer
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseSeventeen" class="collapse show" aria-labelledby="headingSeventeen" data-parent="#general17">
                                        <div class="card-body">
                                            <div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Remarks </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="Label3" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Panel ID="pnlWSDClosed" runat="server" Visible="false">
                                        <div id="general15" class="custom-accordion">

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFifTeen" aria-expanded="true"
                                        aria-controls="collapseFifTeen">
                                        <div class="card-header py-3" id="headingFifTeen">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>FIR Card Closed
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFifTeen" class="collapse show" aria-labelledby="headingFifTeen" data-parent="#general15">
                                        <div class="card-body">
                                           <div>
                                                <div class="form-group">
                                                            <label class="text-primary"><b>Remarks </b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblremarks21" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                 <div class="form-group">
                                                            <label class="text-primary"><b>Updated By</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblUpdated25" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="text-primary"><b>Updated On</b> <span class="text-danger">:</span></label>
                                                            <asp:Label ID="lblUpdated21" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </div>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                    </asp:Panel>
                                </div>


                            </div>
                        </div>

                       





                        <!-- end col -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
