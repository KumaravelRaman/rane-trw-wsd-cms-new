﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Rane_WSD.User.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "dark",
                backgroundColor: "white",
                title: {
                    //text: "Total Registered Users",
                    fontcolor:"black"
                    
                },
                axisY: {
                    //title: "Registered Users",
                    fontcolor:"black"

                },
                Lengend:
        {
            fontcolor:"black"
        },
                data: [{
                    type: "pie",
                    indexLabelFontSize: 10,
                    //showInLegend: true, 
                    //legendMarkerColor: "grey",
                    //legendText: "Registered Users Status",
                    fontColor: "White",
                    dataPoints: <%= chart %> 
                    //    [
                    //    { y: 450 },
                    //    { y: 414 },
                    //    { y: 520, indexLabel: "\u2191 highest", markerColor: "red", markerType: "triangle" },
                    //    { y: 460 },
                    //    { y: 450 },
                    //    { y: 500 },
                    //    { y: 480 },
                    //    { y: 480 },
                    //    { y: 410, indexLabel: "\u2193 lowest", markerColor: "DarkSlateGrey", markerType: "cross" },
                    //    { y: 500 },
                    //    { y: 480 },
                    //    { y: 510 }
                    //]
                    }]
            });
            chart.render();

        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Dashboard</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <!-- Widget  -->
              <% if(Session["UserType"].ToString()=="WSD" || Session["UserType"].ToString()=="Sales Engineer") {%>
            <div class="row">
          
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">New</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_New"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-rev"></div>
                                    <span class="text-success font-weight-bold font-size-13">
                                        <i class="bx bx-up-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Pending</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_pending"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-order"></div>
                                    <span class="text-danger font-weight-bold font-size-13">
                                        <i class="bx bx-down-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Rejected</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_rejected"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-user"></div>
                                    <span class="text-success font-weight-bold font-size-13">
                                        <i class="bx bx-up-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Completed</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_Completed"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-visitor"></div>
                                    <span class="text-danger font-weight-bold font-size-13">
                                        <i class="bx bx-down-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <%} %>
            <!-- Row 2-->
            <div class="row align-items-stretch">
                      <% if(Session["UserType"].ToString()=="WSD") {%>
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Approved FIR</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_WSDServiceApproved"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Rejected FIR </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_WSDServiceRejected"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Warehouse</h5>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">WSD Dispatched to Warehouse</p>
                                         <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_WSDDispatchWarehouse"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                        <i class="bx bx-layer fs-lg"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Received From Warehouse</p>
                                        <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_WSDReceivedWarehouse"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-success mr-0 align-self-center">
                                        <i class="bx bx-bar-chart-alt fs-lg"></i>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
 

            <%} else if(Session["UserType"].ToString()=="Service Engineer") { %>

           
      <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Allocate FIR</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_servenggallocatefir"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Completed FIR </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_servenggcompletedFIR"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
         

                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Approved FIR</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_ServEnggApproved"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Rejected FIR </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_ServEnggRejected"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
             
     
              
    
            <%} else if(Session["UserType"].ToString()=="Sales Engineer") { %>

          
               
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Sales Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Sales Engineer Approved FIR</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_SEApproved"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Sales Engineer Rejected FIR </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_SERejected"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
             <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Sales Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Pending with Admin Approval</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_SEPendingWithAdminApproval"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Material Sent Back to Retailer </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_SEsentback"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            
               
              <%} else if(Session["UserType"].ToString()=="ASC") { %>

           
   
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Allocate ASC/ASC Observation</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_AScallocateFIR"></asp:Label><span class="mb-0 mt-1" style="font-size:25px" >/</span><asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_ASCobserv"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                          
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Feedback Approved/Rejected</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_ASCfeedbackapproved"></asp:Label><span class="mb-0 mt-1" style="font-size:25px" >/</span><asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_ASCfeedbackrejected"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                   
                            </ul>
                        </div>
                    </div>
                </div>
          <div class="col-md-3 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">ASC </h5>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                          
                      
                              <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">WSD Dispatched to WSD</p>
                                         <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_ASCdispatchedtoWSD"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                        <i class="bx bx-layer fs-lg"></i>
                                    </div>
                                </div>
                            </li>
                               <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Material Dispatched to ASC </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lbl_ASCmaterialDispatchedtoASC"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                        </ul>
                    </div>
                </div>
            </div>
                
               

        <%} else if(Session["UserType"].ToString()=="Warehouse") { %>
      
   


          <div class="col-md-4 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Warehouse </h5>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">WSD Dispatched to Warehouse</p>
                                         <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_WHmaterialDispatchedtowh"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                        <i class="bx bx-layer fs-lg"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Material Received From WSD by Warehouse</p>
                                        <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_WHmaterialreceivedfromwsd"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-success mr-0 align-self-center">
                                        <i class="bx bx-bar-chart-alt fs-lg"></i>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
                <div class="col-md-4 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Warehouse </h5>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Credit Note Updated</p>
                                         <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_whcreditnoteupdated"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                        <i class="bx bx-layer fs-lg"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Completed</p>
                                        <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lbl_whcompleted"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-success mr-0 align-self-center">
                                        <i class="bx bx-bar-chart-alt fs-lg"></i>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
    
    <%} %>
            
                   <div class="col-md-6 col-lg-6">
                    
            <div class="card">
               
                    
            <div id="chartContainer"></div>
                   
            </div>
            <!-- End total revenue chart -->
        </div>
            </div>
        

        <!-- Begin total revenue chart -->
        
   </div>
   </div>
        </div>
                  <!-- Row 3-->
</asp:Content>
