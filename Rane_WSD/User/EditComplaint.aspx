﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="EditComplaint.aspx.cs" Inherits="Rane_WSD.User.EditComplaint" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
</script>
   <style>
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
</style>--%>

    <style type="text/css">
        .modal-content {
            width: 60%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Update FIR </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Update FIR </li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Update FIR  <button type="button" class="btn btn-info float-right btn"  data-toggle="modal" id="InfoAlert"
                                    data-target="#info-alert-modal">
                                    Info Alert</button>
 </h5>



                        </div>
                        <div class="card-body">






                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>




                            <div id="general2" class="custom-accordion" >

                                <div class="card shadow-none mb-3" style="display:none;">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true"
                                        aria-controls="collapseFour">
                                        <div class="card-header py-3" id="headingFour">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Note
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#general2">
                                        <div class="card-body">
                                            <p><b style="color: Grey">Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.</b></p>
                                        </div>
                                    </div>
                                </div>


                                <div class="card shadow-none mb-3">
                                    <a data-toggle="collapse" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                        <div class="card-header py-3" id="headingFive">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-information text-primary h5 mr-3"></i>FIR Details
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#general2">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-4">

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>FIR No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblFirno" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtFirno" ForeColor="Red" ReadOnly="true" class="form-control" runat="server" placeholder="Enter FIRNo "></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtFirno" runat="server" ErrorMessage="Enter FIRNo"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Name </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblWSDName" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtWSDname" ReadOnly="true" class="form-control" runat="server" ForeColor="Blue" placeholder="Enter WSDName"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtWSDname" runat="server" ErrorMessage="Enter WSDName"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Branch </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblWSDBranch" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtWSDBranch" ReadOnly="true"  class="form-control" runat="server" ForeColor="Blue"  placeholder="Enter WSDBranch"></asp:TextBox>--%>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtWSDBranch" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Aggregate Serial No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblAggregateSerialNo" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtSerialno" ReadOnly="true" placeholder="Enter Aggregate Serial No" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtSerialno" ErrorMessage="Enter AggregateSerial No"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Upload File </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblUploadfile" style="display:none;" runat="server"></asp:Label>
                                                        <%-- <asp:FileUpload ID="FileUpload1" AllowMultiple="true" class="form-control" runat="server" />--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" ForeColor="Red" ControlToValidate="FileUpload1" runat="server" ErrorMessage="Choose File"></asp:RequiredFieldValidator>--%>
                                                        <%-- <asp:Label ID="listofuploadedfiles" runat="server" Text=""></asp:Label>--%>
                                                        <div class="row">
                                                            <asp:Panel ID="pnlPDF" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName %>"  data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF1" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName1 %>"  data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label3" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF2" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName2 %>" data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label4" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                                    <i class="fas fa-file-pdf"></i>
                                                                </a>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlPDF3" runat="server">
                                                                <a target="_blank" href="../Upload/<%= pdfName3 %>" data-toggle="tooltip" data-placement="top" style="color:#d34372">
                                                                    ,<asp:Label ID="Label5" Font-Bold="true" runat="server" Text=""></asp:Label>,
                                                                    <i class="fas fa-file-pdf"></i>,
                                                                </a>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-4">
                                                    <div class="form-group">

                                                        <label class="text-primary"><b>Retailer Name </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblretailername" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtRetailerName" ReadOnly="true" class="form-control" runat="server" placeholder="Enter Retailer Name"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" ControlToValidate="txtRetailerName" runat="server" ErrorMessage="Enter Retailer Name"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Retailer Location </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblretailerlocation" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtRetailerLocation"  ReadOnly="true" class="form-control" runat="server" placeholder="Enter Retailer Location"></asp:TextBox>--%>


                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="txtRetailerLocation" runat="server" ErrorMessage="Enter Retailer Location"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>WSD Invoice No </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblInvoiceno" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtInvoiceNo" class="form-control" ReadOnly="true" runat="server" placeholder="Enter WSD Invoice No"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="Red" ControlToValidate="txtInvoiceNo" runat="server" ErrorMessage="Enter WSDInvoice No"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Date of Complaint </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblDateofComplaint" runat="server"></asp:Label>
                                                        <%--<asp:TextBox ID="txtDateofComplaint" ReadOnly="true"  class="form-control" runat="server" TextMode="Date" placeholder="Enter Date "></asp:TextBox>--%>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" ForeColor="Red" runat="server" ControlToValidate="txtDateofComplaint" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Invoice Date</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblInvoiceDate" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtInvoicedate" ReadOnly="true" TextMode="Date" placeholder="Enter Invoice Date" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" runat="server" ControlToValidate="txtInvoicedate" ErrorMessage="Enter Invoice Date"></asp:RequiredFieldValidator>--%>
                                                    </div>



                                                </div>
                                                <!-- end col -->

                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Part No</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblpartno" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtpartno" class="form-control" ReadOnly="true" runat="server" placeholder="Enter Part No"></asp:TextBox>--%>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ControlToValidate="txtpartno" ErrorMessage="Enter Part No"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">

                                                        <label class="text-primary"><b>Part Type </b><span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblparttype" runat="server"></asp:Label>
                                                        <%-- <asp:DropDownList ID="ddlParttype" AutoPostBack="true" OnSelectedIndexChanged="ddlParttype_SelectedIndexChanged" ReadOnly="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Aggregate">Aggregate</asp:ListItem>
                                            <asp:ListItem Value="Child Part">Child Part</asp:ListItem>
                                        </asp:DropDownList>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="ReqParttype" ForeColor="Red" ControlToValidate="ddlParttype" runat="server" ErrorMessage="Choose PartType"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Quantity</b> <span class="text-danger">:</span></label>
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                        <%-- <asp:TextBox ID="txtQuantity" ReadOnly="true"  class="form-control" runat="server" placeholder="Enter Quantity"></asp:TextBox>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ForeColor="Red" ControlToValidate="txtQuantity" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>--%>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="text-primary"><b>Status :</b></label>
                                                        <asp:Label ID="lblStatus" CssClass="text-info" Font-Bold="true"  runat="server"></asp:Label>
                                                        <%-- <asp:DropDownList ID="ddlStatus" ReadOnly="true" class="form-control" runat="server">
                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Deactive">Deactive</asp:ListItem>
                                        </asp:DropDownList>--%>
                                                    </div>


                                                </div>
                                                <div class="pl-3">
                                                    <%-- <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Update" />--%>
                                                    <%--  <a href="ListComplaint.aspx" class=" btn btn-dark btn-sm" >Back</a>--%>
                                                </div>
                                                <!-- end col -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- collapse one end -->
                                  
                                <div id="general7" class="custom-accordion">
                                     <asp:Panel ID="pnlASCObservation" Visible="false" runat="server">
                                    <div class="card shadow-none mb-3">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseNine" aria-expanded="true"
                                            aria-controls="collapseNine">
                                           
                                            <div class="card-header py-3" id="headingNine">
                                                <h6 class="mb-0">
                                                    <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>ASC Observation
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                </h6>
                                            </div>
                                                 

                                        </a>
                                      
                                            <div id="collapseNine" class="collapse show" aria-labelledby="headingNine" data-parent="#general7">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>ASC FeedBack  :</b></label>
                                                                <asp:Label ID="lblFeedback" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>ASC Recommendation  :</b></label>
                                                                <asp:Label ID="lblRecommendation" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="text-primary"><b>ASC Reported Date :</b></label>
                                                                <asp:Label ID="lblReported" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                      
                                       
                                    </div>
                                           </asp:Panel>
                                </div>

                                <div class="card shadow-none mb-3">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="true"
                                        aria-controls="collapseSix">
                                        <div class="card-header py-3" id="headingSix">
                                            <h6 class="mb-0">
                                                <i class="mdi mdi-refresh-circle text-primary h5 mr-3"></i>Update FIR
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                            </h6>
                                        </div>
                                    </a>

                                    <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#general2">
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-lg-4">
                                                    <asp:Panel ID="pnlStatus" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Status </b><span class="text-danger">*</span></label>

                                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="ddlStatus" runat="server" ErrorMessage="Choose Status"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>


                                                    <asp:Panel ID="pnlObservation" runat="server">

                                                        <div class="form-group">
                                                            <label><b>Observation of Problem</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtObservation"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqObservation" ForeColor="Red" runat="server" ControlToValidate="txtObservation" ErrorMessage="Enter Observation"></asp:RequiredFieldValidator>

                                                        </div>

                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlReason" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Reason For Rejection </b><span class="text-danger">*</span></label>

                                                            <asp:TextBox ID="txtReason" TextMode="MultiLine" placeholder="Enter Remarks" CssClass="form-control" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" runat="server" ControlToValidate="txtReason" ErrorMessage="Enter Remarks"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlASCUpadte" runat="server">
                                                        <div class="form-group">
                                                            <asp:Label runat="server" ID="lblASCName" Font-Bold="true" Text="ASC Name"></asp:Label>
                                                            <asp:TextBox runat="server" ID="txtASCName" ForeColor="Blue" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                        </div>

                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlCreditValue" runat="server">
                                                         <div class="form-group">
                                                            <label><b>Value</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtValue" TextMode="Number" runat="server" CssClass="form-control" placeholder="Enter Value"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ForeColor="Red" ControlToValidate="txtValue" runat="server" ErrorMessage="Enter Value"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlServiceEng" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Service Engineer </b><span class="text-danger">*</span></label>

                                                            <asp:DropDownList ID="ddlserviceEng" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="ddlserviceEng" runat="server" ErrorMessage="Choose Status"></asp:RequiredFieldValidator>

                                                        </div>
                                                    </asp:Panel>
                                                    <div class="form-group">
                                                        <asp:Label ID="lblRemarks" Font-Bold="true" runat="server" Text="Remarks"><span class="text-danger">*</span></asp:Label>

                                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" placeholder="Enter Remarks" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtRemarks" ErrorMessage="Enter Remarks"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <asp:Panel ID="pnlCreditNote" runat="server">
                                                        <div class="form-group">
 <label><b>Credit Note No</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtCreditNote" runat="server" CssClass="form-control" placeholder="Enter Credit Note No"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ForeColor="Red" runat="server" ControlToValidate="txtCreditNote" ErrorMessage="Enter Credit Note No"></asp:RequiredFieldValidator>
</div>
                                                        <div class="form-group">
                                                            <label><b>Plant Document No</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtplantDocuno"  runat="server" CssClass="form-control" placeholder="Enter Plant Document No"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ForeColor="Red" ControlToValidate="txtplantDocuno" runat="server" ErrorMessage="Enter Plant Document No"></asp:RequiredFieldValidator>
                                                        </div>
                                                        
                                                         <div class="form-group">
                                                            <label><b>Plant Notification No</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtplantnotify"  runat="server" CssClass="form-control" placeholder="Enter Plant Notification No"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ForeColor="Red" ControlToValidate="txtplantnotify" runat="server" ErrorMessage="Enter Plant Notification No"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlInwardNo" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Inward Number</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtinwardnumber" runat="server" CssClass="form-control" placeholder="Enter Inward Number"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ForeColor="Red" runat="server" ControlToValidate="txtinwardnumber" ErrorMessage="Enter Inward Number"></asp:RequiredFieldValidator>
                                                        </div>
                                                         <div class="form-group">
                                                            <label><b>Inward Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtinwarddate" runat="server" TextMode="Date" CssClass="form-control" placeholder="Enter Inward Date"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ForeColor="Red" runat="server" ControlToValidate="txtinwarddate" ErrorMessage="Enter Inward Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnlWSDupdate1" runat="server">
                                                         <div class="form-group">
                                                            <label><b>Upload Files</b></label>
                                                            <asp:FileUpload ID="FileUpload2" CssClass="form-control" AllowMultiple="true" runat="server" />
                                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ForeColor="Red" runat="server" ControlToValidate="FileUpload2" ErrorMessage="Please Choose Files"></asp:RequiredFieldValidator>--%>
                                                            <asp:Label ID="listofuploadedfiles1" runat="server"></asp:Label>
                                                              <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Bold="true" Text="Upload Maximum 4 Files"></asp:Label>
                                                       
                                                         <asp:RegularExpressionValidator ID="UploadFile" runat="server"
                                    ControlToValidate="FileUpload2" Display="Dynamic"
                                    ErrorMessage="Only pdf,doc,img file are allowed"
                                    ValidationExpression="^.+(.pdf|.PDF|.jpg|.JPG|.png|.PNG|.doc|.DOC|.docx|.DOCX|.jpeg|.JPEG)$"
                                    Font-Bold="True" Font-Italic="True" ForeColor="#CC3300" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                              </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlWarrehouse" runat="server">
                                                        <div class="form-group">
                                                            <label><b>LR No</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="textLRNO" runat="server" CssClass="form-control" placeholder="Enter LR No"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ForeColor="Red" ControlToValidate="textLRNO" runat="server" ErrorMessage="Enter LR No"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><b>Delivery Note No:</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtDeliveryNote" runat="server" CssClass="form-control" placeholder="Enter Delivery Note No"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ForeColor="Red" ControlToValidate="txtDeliveryNote" runat="server" ErrorMessage="Enter Delivery Note No"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlWarranty" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Service Eng Recommendation</b><span class="text-danger">*</span></label>
                                        <asp:DropDownList ID="ddlWarranty" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="Select">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="No Scope of Warranty">No Scope of Warranty</asp:ListItem>
                                                                <asp:ListItem Value="Scope of Warranty">Scope of Warranty</asp:ListItem>

                                        </asp:DropDownList>  
                                                            <asp:RequiredFieldValidator ID="reqSerRecommendation" runat="server" ControlToValidate="ddlWarranty"   ForeColor="Red" ErrorMessage="Choose ServiceEng Observation"></asp:RequiredFieldValidator> 
                                                        </div>

                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlASC" runat="server">
                                                        <div class="form-group">
                                                            <label><b>ASC </b><span class="text-danger">*</span></label>

                                                            <asp:DropDownList ID="ddlASC" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" ControlToValidate="ddlASC" runat="server" ErrorMessage="Choose ASC"></asp:RequiredFieldValidator>

                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlLRNo" runat="server">
                                                        <div class="form-group">
                                                            <label><b>LR No</b
                                                                ></label><span class="text-danger">*</span>
                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtLRNo" placeholder="Enter LR No"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqLRNo" ForeColor="Red" runat="server" ControlToValidate="txtLRNo" ErrorMessage="Enter LR No"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><b>DC No</b></label><span class="text-danger">*</span>
                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtDcno" placeholder="Enter DC No"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqDcNo" ForeColor="Red" runat="server" ControlToValidate="txtDcno" ErrorMessage="Enter DC No"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlReportedDate" runat="server">
                                                        <div class="form-group">
                                                            <label><b>ASC Observation</b><span class="text-danger">*</span></label>
                                                            <asp:DropDownList ID="ddlObservation" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="Select">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="No Scope of Warranty">No Scope of Warranty</asp:ListItem>
                                                                <asp:ListItem Value="Scope of Warranty">Scope of Warranty</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="ddlObservation" runat="server" ErrorMessage="Choose ASC Observation"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="form-group">
                                                            <label><b>Upload Files</b><span class="text-danger">*</span></label>
                                                            <asp:FileUpload ID="FileUpload1" CssClass="form-control" AllowMultiple="true" runat="server" />
                                                            
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" runat="server" ControlToValidate="FileUpload1" ErrorMessage="Upload Files"></asp:RequiredFieldValidator>
                                                          
                                                             <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="true" Text="Upload Maximum 4 Files"></asp:Label>
                                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="FileUpload1" Display="Dynamic"
                                    ErrorMessage="Only pdf,doc,img file are allowed (Upload Maximum 4 Files Only)"
                                    ValidationExpression="^.+(.pdf|.PDF|.jpg|.JPG|.png|.PNG|.doc|.DOC|.docx|.DOCX|.jpeg|.JPEG)$"
                                    Font-Bold="True" Font-Italic="True" ForeColor="#CC3300" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                            <asp:Label ID="listofuploadedfiles" runat="server"></asp:Label>
                                                          
                                                        </div>
                                                         
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlMethod" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Dispatch Method Type</b><span class="text-danger">*</span></label>
                                                            <asp:DropDownList ID="ddlmethodtype" AutoPostBack="true" OnSelectedIndexChanged="ddlmethodtype_SelectedIndexChanged" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="Select">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Courier">Courier</asp:ListItem>
                                                                <asp:ListItem Value="By Hand">By Hand</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="ReqMethodtype" ForeColor="Red" ControlToValidate="ddlmethodtype" runat="server" ErrorMessage="Choose Method Type"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlDocu" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Document No</b></label><span class="text-danger">*</span>
                                                            <asp:TextBox ID="txtdocno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="txtdocno" runat="server" ErrorMessage="Enter Document No"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <%--  <div class="form-group">
                                              <label><b>Date</b></label><span class="text-danger">*</span>
                                                        <asp:TextBox ID="txtdate" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="txtdate" runat="server" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>
                                                            </div>--%>
                                                    </asp:Panel>

                                                </div>
                                                <div class="col-lg-4">
                                                    <asp:Panel ID="pnlPlantNo" runat="server">
                                                        <div class="form-group">
                                                             <label><b>Credit Note Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtCreditDate" TextMode="Date" runat="server" CssClass="form-control" placeholder="Enter Credit Note Date"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ForeColor="Red" runat="server" ControlToValidate="txtCreditDate" ErrorMessage="Enter Credit Note Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                        
                                                         <div class="form-group">
                                                            <label><b>Plant Document Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtplantdocudate" TextMode="Date" runat="server" CssClass="form-control" placeholder="Enter Plant Document Date"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator22" ForeColor="Red" ControlToValidate="txtplantdocudate" runat="server" ErrorMessage="Enter Plant Document Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                       
                                                    </asp:Panel>
                                                   
                                                    <asp:Panel ID="pnlQuantity" runat="server">
                                                        <div class="form-group">
                                                            <label><b>Quantity</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtQuantity" TextMode="Number" runat="server" CssClass="form-control" placeholder="Enter Quantity"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ForeColor="Red" ControlToValidate="txtQuantity" runat="server" ErrorMessage="Enter Quantity"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlWSDSend" runat="server">
                                                         <div class="form-group">
                                                            <label><b>LR Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="textLRDate" TextMode="Date" runat="server" CssClass="form-control" placeholder="Enter LR Date"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ForeColor="Red" ControlToValidate="textLRDate" runat="server" ErrorMessage="Enter LR Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                        
                                                         <div class="form-group">
                                                            <label><b>Transportation Name :</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txttransportation" runat="server" CssClass="form-control" placeholder="Enter Transportation Name"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ForeColor="Red" ControlToValidate="txttransportation" runat="server" ErrorMessage="Enter Transportation Name"></asp:RequiredFieldValidator>
                                                        </div>
                                                        
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnldateASC" runat="server">
                                                        <div class="form-group">
                                                            <label><b>ASC Reported Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtreporteddate" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="Red" ControlToValidate="txtreporteddate" runat="server" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlLRDate" runat="server">
                                                        <div class="form-group">
                                                            <label><b>LR Date</b><span class="text-danger">*</span></label>
                                                            <asp:TextBox ID="txtLRDate" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqLRdate" ForeColor="Red" ControlToValidate="txtLRDate" runat="server" ErrorMessage="Enter LR Date"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="text-center">
                                                        <asp:Button runat="server" CssClass="btn btn-primary " ID="btnSubmit" OnClick="btnSubmit_Click" Text="Update" />
                                                        <a href="ListComplaint.aspx" class=" btn btn-dark ">Back</a>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div id="info-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="text-center">
                                                    <i class="bx bx-info-circle fs-xl text-info"></i>
                                                    <h4 class="mt-2">Notes !</h4>
                                                    <p class="mt-3">
                                                        Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.
                                                    </p>
                                                    <button type="button" id="btnDismiss" class="btn btn-info my-2" data-dismiss="modal">Continue</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- collapse two end -->



                            </div>
                            <!-- end #accordions-->
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
