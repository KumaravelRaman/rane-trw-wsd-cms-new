﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class CreateFIR : System.Web.UI.Page
    {
        public string FIRCode = "";
        public bool noError = true;
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtWSDBranch.Text = Session["Location"].ToString();
            txtWSDname.Text = Session["BranchName"].ToString();
            //SerialNo.Visible = false;
            if (!IsPostBack)
            {
                getno();
                GetData();
            }


        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct [Id],FIRStatus from [tbl_FIRstatus] where Id in (1)";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlStatus.DataSource = dt;
            ddlStatus.DataTextField = "FIRStatus";
            ddlStatus.DataValueField = "Id";
            ddlStatus.DataBind();
            string defaultValue = "1";
            ddlStatus.DataValueField = defaultValue;
            //ddlUsertype.SelectedValue = usertype;
            // ddlUsertype.Enabled = false;
        }
        protected void getno()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            string queryy = @"select count(Id)cnt from tbl_FIRRegister";
            SqlCommand cmd = new SqlCommand(queryy, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dtt = new DataTable();
            sda.Fill(dtt);


            if (dtt.Rows.Count > 0)
            {
                string prefix = "FIR";
                DateTime Date = DateTime.Now.Date;
                string Datee = Date.ToString("yyyy");

                int suffix = Convert.ToInt16(dtt.Rows[0]["cnt"]);
                suffix = suffix + 1;
                Session["suffix"] = suffix;
                string conn = "000";
                FIRCode = prefix + '/' + Datee + '/' + conn + suffix;
                //FIRCode = conn + suffix + '/' + Datee;
            }
            txtFirno.Text = FIRCode;
            Session["FIRCode"] = FIRCode;
            con.Close();
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            CreateComplaint();

            if (txtAggregateModel.Text != string.Empty || txtSerialno.Text != string.Empty)
            {

                if (noError)
                {
                    Response.Redirect("FIRCode.aspx");
                }
                else
                {
                    //pnlerror.Visible = true;
                    lblError.Text = "Error Occured please Contact Administrator";
                }
            }
            else
            {
                Response.Write("<script language='javascript'>alert('Enter all Credintals');window.location=('CreateFIR.aspx')</script>");
            }
        }
        protected void CreateComplaint()
        {
            ddlStatus.SelectedValue = "1";
            ddlStatus.Enabled = true;
            SqlConnection con = new SqlConnection(cs);
            try
            {

                // string path1 = "~/Upload/" + FileUpload1.FileName;

                //if (Path.GetExtension(path1) == ".jpg" || Path.GetExtension(path1) == ".png" || Path.GetExtension(path1) == ".jpeg"||Path.GetExtension(path1) == ".jpg" ||Path.GetExtension(path1) == ".pdf" ||Path.GetExtension(path1) == ".doc" ||Path.GetExtension(path1) == ".docx" )
                //{
                //    FileUpload1.PostedFile.SaveAs(Server.MapPath(path1));
                //}

               

                SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);
                SqlCommand cmd1 = new SqlCommand("sp_b_FIRDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd1.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "insertFIR");
                cmd1.Parameters.AddWithValue("@qtype", "insFIR");
                cmd.Parameters.AddWithValue("@FIRNo", txtFirno.Text.Trim());
                //cmd1.Parameters.AddWithValue("@FIRId", );
                cmd1.Parameters.AddWithValue("@FIRNo", txtFirno.Text.Trim());
                cmd.Parameters.AddWithValue("@WSDName", txtWSDname.Text.Trim());
                cmd.Parameters.AddWithValue("@WSDBranch", txtWSDBranch.Text.Trim());
                cmd.Parameters.AddWithValue("@RetailerName", txtRetailerName.Text.Trim());
                cmd.Parameters.AddWithValue("@RetailerLocation", txtRetailerLocation.Text.Trim());
                cmd.Parameters.AddWithValue("@PartNo", txtpartno.Text.Trim());
                cmd.Parameters.AddWithValue("@PartType", ddlParttype.SelectedValue);
                cmd.Parameters.AddWithValue("@WSDInvoiceNo", txtInvoiceNo.Text.Trim());
                cmd.Parameters.AddWithValue("@AggregateSerialNo", txtSerialno.Text.Trim());
                cmd.Parameters.AddWithValue("@AggregateModel", txtAggregateModel.Text.Trim());
                cmd.Parameters.AddWithValue("@Iswarranty", lblwarranty.Text.Trim());
                cmd.Parameters.AddWithValue("@InvoiceDate", txtInvoicedate.Text.Trim());
                cmd.Parameters.AddWithValue("@Quantity", txtQuantity.Text.Trim());

                cmd.Parameters.AddWithValue("@Notes", txtNotes.Text.Trim());

                if (FileUpload1.HasFiles)
                {
                    foreach (HttpPostedFile uploadedFile in FileUpload1.PostedFiles)
                    {
                        uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Upload/"), uploadedFile.FileName));
                        listofuploadedfiles.Text += String.Format("{0},", uploadedFile.FileName);

                    }
                    cmd.Parameters.AddWithValue("@UploadFile", listofuploadedfiles.Text);
                }

                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);

                cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());
                cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();


            }
        }

        protected void ddlParttype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlParttype.SelectedValue == "Aggregate")
            {
                txtQuantity.Text = "1";
                txtQuantity.ReadOnly = true;
                SerialNo.Visible = true;
                //Response.Write("<script>jsFunction('Aggregate'); </script>");
            }
            else if (ddlParttype.SelectedValue == "Child Part")
            {
                txtQuantity.ReadOnly = false;
                SerialNo.Visible = false;
            }
            else
            {
                lblError.Text = "Choose Part Type";
            }
        }

        protected void txtInvoicedate_TextChanged(object sender, EventArgs e)
        {
            // int result = DateTime.Compare(DateTime.Now.ToString("yyyy-mm-dd"), Convert.ToDateTime(txtInvoicedate.Text.Trim()));

            // Response.Write(result);

            var today = DateTime.Now;
            var invoicedate = Convert.ToDateTime(txtInvoicedate.Text.Trim());

            int result = (today.Year * 12 + today.Month) - (invoicedate.Year * 12 + invoicedate.Month);
            

            if (result > 9)
            {
                txtdeviation.Text = "Out of Warranty!!!,Please Wait For Admin Approval";

                lblwarranty.Text = "1";
            }
            else
            {
                lblwarranty.Text = "0";
            }
            // (start.Year * 12 + start.Month) - (end.Year * 12 + end.Month);


            //Response.Write(DateTime.Now.ToString("yyyy-mm-dd"));
            //Response.Write(txtInvoicedate.Text.Trim());


            //if (result < 0)
            //    Console.WriteLine("issue date is less than expired date");
            //else if (result == 0)
            //    Console.WriteLine("Both dates are same");
            //else if (result > 0)
            //    Console.WriteLine("issue date is greater than expired date");

            //SqlConnection con = new SqlConnection(cs);
            //con.Open();
            //SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@qtype", "AdminApproval");
            //cmd.Parameters.AddWithValue("@InvoiceDate", Convert.ToString(txtInvoicedate.Text.Trim()));
            //SqlDataReader sdr = cmd.ExecuteReader();
            //if (sdr.Read())
            //{
            //    string DeviationMonth = sdr["DeviationMonth"].ToString();

            //    if (DeviationMonth == "9")
            //    {
            //        txtdeviation.Text = "Out of Warranty!,Please Wait For Admin Approval";
            //    }
            //    else
            //    {

            //    }
            //}
            //con.Close();

        }
    }
}
