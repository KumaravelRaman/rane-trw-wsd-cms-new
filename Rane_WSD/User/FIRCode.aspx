﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="FIRCode.aspx.cs" Inherits="Rane_WSD.User.FIRCode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold"> FIR Code</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">  FIR Code</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                           
                            
                        </div>
                        <div class="card-body">
                             
                          <div class="text-center">
                              <%--<img src="../Images/FIRImages.jpg" class="rounded-circle" style="width:30%"/>--%>
                             <img src="../Images/Online-FIR-in-India.png" class="rounded-circle" style="width:30%"/>
                          </div>
                            <div class="text-center">
                                <b style="font-size:22px;font-family:cursive;">Your Complaint Raised Successfully!!!</b>

                            </div>
                           
                            <div class="text-center">
                            <div class="btn btn-secondary">
                                <b style="font-size:15px;font-family:cursive;">Your Ticket No </b> <span class="text-light">:</span>
                                <asp:Label ID="Label1" runat="server" style="font-size:15px;font-family:cursive;"  Text=""></asp:Label>
                            </div>
                               </div>
                                
                            <!-- end row-->
                            <div style="margin-top:10px;">
                            <div class="text-center">
                                    
                                      <a href="ListComplaint.aspx" class=" btn btn-success btn-sm " >Okay</a>
                                </div>
                                </div>
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
