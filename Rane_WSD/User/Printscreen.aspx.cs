﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class Printscreen : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "Selectdata");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblFIRNo.Text = sdr["FIRNo"].ToString();
                    lblWSDName.Text = sdr["WSDName"].ToString();
                    lblWSDBranch.Text = sdr["WSDBranch"].ToString();
                    lblDateofComplaint.Text = String.Format("{0:dd MMM yyyy h:mm tt}", sdr["DateofComplaint"]); 
                    lblRetailerName.Text = sdr["RetailerName"].ToString();
                    lblRetailerLocation.Text = sdr["RetailerLocation"].ToString();
                    lblWSDInvoiceNo.Text = sdr["WSDInvoiceNo"].ToString();
                    lblInvoiceDate.Text = String.Format("{0:dd MMM yyyy }", sdr["InvoiceDate"]); 
                    lblPartNo.Text = sdr["PartNo"].ToString();
                    lblAggregateSerialNo1.Text = sdr["AggregateSerialNo"].ToString();
                    lblStatus1.Text = sdr["StatusName"].ToString();
                    lblallocatedServiceEng1.Text = sdr["AllocateServiceEngg"].ToString();
                    lblAllocatedASC1.Text=sdr["AllocateAsc"].ToString();
                    lblASCRecommendation1.Text = sdr["ASCRecommendation"].ToString();
                    lblASCRepotedDate1.Text = String.Format("{0:dd MMM yyyy }", sdr["ASCReportedDate"]);
                    lblObservationofProblem1.Text = sdr["ObservationofProblem"].ToString();
                    lblServiceEngRecommendation1.Text = sdr["ASCRecommendationService"].ToString();
                    lblRTSSASCName.Text = sdr["AllocateAsc"].ToString();
                    lblCustomerComplaint.Text = sdr["Notes"].ToString();
                    lblServiceApproveReject.Text = sdr["ServiceEnggApproval"].ToString();
                    lblAggregateModel.Text = sdr["AggregateModel"].ToString();
                }
                con.Close();

                    

            }
        }
    }
}