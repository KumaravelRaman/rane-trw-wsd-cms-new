﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="ListComplaint.aspx.cs" Inherits="Rane_WSD.User.ListComplaint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $("#mytable tr").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">List of FIR</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">List of Register FIR</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">FIR List</h5>
                            <h6 class="card-subtitle"></h6>
                        </div>
                        <div class="card-body">
                            <asp:Label ID="lblError" ForeColor="Red" runat="server" Text=""></asp:Label>


                               <%-- <div class="col-md-4">
                                    <asp:HyperLink ID="hplCreate" Visible="false" class="btn btn-success btn-sm" NavigateUrl="~/User/CreateFIR.aspx" runat="server">Create New</asp:HyperLink>
                                    
                                </div>--%>

                             <div class="card">
                                     <div class="btnsprimary col-md-12 pull-left">
                    <div class="row">
                        <div class="col-md-3 ">
                            <asp:TextBox runat="server" ID="txtFIRNO" placeholder="Search By FIRNo..." CssClass="form-control"></asp:TextBox>
                            </div>
                         <div class="col-md-3 ">
                            <asp:TextBox runat="server" ID="txtWSDName" placeholder="Search by PartNo ..." CssClass="form-control"></asp:TextBox>
                            </div>
                         <div class="col-md-3 ">
                             <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                         
                        <div class="col-md-3 pt-1">
                            <asp:Button class="btn btn-primary" ID="Button1" ValidationGroup="btnSearch" Text="Search" runat="server" OnClick="btn_submit_Click" />


                        </div>
               
                </div>
                                         </div>
                    </div>
                      
                            <%--<div class="col-md-1 offset-md-9">
                                Search :
                            </div>
                            <div class="col-md-3 pb-2 float-right">
                                <input class="form-control" id="myInput" type="text" placeholder="Type Something..." />
                            </div>--%>
                            <div class="table-responsive">
                                <asp:ListView ID="ListView1" runat="server" DataKeyNames="Id" ItemPlaceholderID="itmPlaceholder" OnPagePropertiesChanging="OnPagePropertiesChanging" ConvertEmptyStringToNull="true" OnItemDeleting="ListView1_ItemDeleting"   OnItemDataBound="ListView1_ItemDataBound" >
                                    <LayoutTemplate>
                                        <table
                                            class="table table-bordered table-hover">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center" style="width: 5%;">S.No</th>
                                                    <th class="text-center" style="width:20%;">FIR No</th>
                                                   <%-- <th class="text-center" style="width:20%" >WSD Name</th>--%>
                                                     <th class="text-center" style="width:20%" >Part No</th>
                                                     <th class="text-center" style="width:20%" >Part type</th>
                                                     <th class="text-center" style="width:20%" >Aggregate Serial No</th>
                                                   <%-- <th class="text-center" style="width: 20%;" >WSD Branch</th>--%>
                                                    <th class="text-center" style="width: 10%;">Date of Complaint</th>
                                                    <%-- <th class="text-center">Part Type</th>--%>
                                                    <th class="text-center" style="width: 5%;">Status</th>
                                                    <th class="text-center" style="width: 5%;">Print FIR</th>
                                                    <th class="text-center" style="width: 15%;">Action</th>
                                                </tr>
                                                <tr>
                                                    <asp:PlaceHolder ID="itmPlaceholder" runat="server"></asp:PlaceHolder>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="5">
                                                    <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                            <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                            <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                                <td colspan="5" class="number_of_record" style="text-align: right">
                                                    <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:TemplatePagerField>
                                                                <PagerTemplate>
                                                                    <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                                    </span>
                                                                </PagerTemplate>
                                                            </asp:TemplatePagerField>
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                            </tr>

                                        </table>

                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tbody id="mytable">
                                            <tr>
                                                <td class="text-center">
                                                    <asp:Label ID="Label1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem,"slNo")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text='<%#Eval("FIRNo")%>' />

                                                </td>
                                                <td class="text-center">
                                                    <%--<asp:Label ID="Label4" runat="server" Text='<%#Eval("WSDName")%>'></asp:Label>--%>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("PartNo")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                   <%-- <asp:Label ID="Label3" runat="server" Text='<%#Eval("WSDBranch")%>'></asp:Label>--%>
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("PartType")%>'></asp:Label>
                                                 
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("AggregateSerialNo")%>'></asp:Label>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("DateofComplaint" ,"{0: dd MMM yyyy}")%>'></asp:Label>
                                                </td>
                                                <%--                                                <td class="text-center">
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("PartType")%>'></asp:Label>
                                                </td>--%>
                                                <td class="text-center">
                                                    <asp:HiddenField ID="hfStatus" Value='<%#Eval("Status") %>' runat="server" />
                                                    <asp:HiddenField ID="hfName" Value='<%# Eval("CreatedBy") %>' runat="server" />
                                                    <asp:HiddenField ID="hfServiceEng" Value='<%# Eval("AllocateServiceEngg") %>' runat="server" />
                                                    <asp:HiddenField ID="hfASC" Value='<%# Eval("AllocateAsc") %>' runat="server" />
                                                      <asp:HiddenField ID="hfWSDName" Value='<%# Eval("WSDUser") %>' runat="server" />
                                                    <asp:HiddenField ID="hfInvoiceDate" Value='<%# Eval("InvoiceDate") %>' runat="server" />
                                                    <asp:HiddenField ID="hfwarranty" Value='<%# Eval("Iswarranty") %>' runat="server" />
                                                    
                                                    <asp:Panel ID="pnlStatus" Visible='<%# (Eval("Iswarranty").ToString() == "True") %>' runat="server">
                                                        <span class="text-danger"><b>Pending with admin approval</b></span>
                                                    </asp:Panel>

                                                    <asp:Panel ID="Panel1" Visible='<%# (Eval("Iswarranty").ToString() == "False") %>' runat="server">
                                                        <asp:Label ID="Label5" runat="server" Text='<%# db.IsActive(Eval("StatusName").ToString())%>'></asp:Label>
                                                    </asp:Panel>


                                                    

                                                </td>
                                                <td class="text-center">
                                                    <a href="Printscreen.aspx?Id=<%# Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="Print FIR" data-toggle="tooltip" data-placement="top" class="btn btn-danger btn-sm "><i class="fa fa-print"></i></a>
                                                </td>
                                                <td class="text-center">
                                                    <asp:Panel runat="server" ID="pnlEdit" Visible="false" >
                                                   <%-- <asp:Panel runat="server" Visible='<%# (Session["UserType"].ToString() == "Sales Engineer" && Eval("Status").ToString()=="1" )|| (Session["Name"].ToString() == Eval("AllocateServiceEngg").ToString() && Eval("Status").ToString() =="2")|| (Session["UserName"].ToString() == Eval("UpdatedBy").ToString()&& Eval("Status").ToString()=="5" && Session["UserType"].ToString()=="WSD" )|| (Session["InchargeName"].ToString() == Eval("AllocateAsc").ToString() && Eval("Status").ToString()=="6")||(Session["InchargeName"].ToString() == Eval("AllocateAsc").ToString() && Eval("Status").ToString()=="7") %>'  ID="Panel1">--%>
                                                        <a href="EditComplaint.aspx?Id=<%# Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="Update FIR" data-toggle="tooltip" data-placement="top" class="btn btn-warning btn-sm "><i class="fa fa-edit"></i></a>
                                                        </asp:Panel>
                                              

                                                    <%--</asp:Panel>--%>
                                                   
                                                    <%-- <asp:LinkButton ID="lnkDelete" ToolTip="Delete Complaint" CommandName="delete" CommandArgument='<%# Eval("Id") %>' class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" runat="server" Text='Delete Complaint' OnClientClick='javascript:return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></asp:LinkButton>--%>
                                                    <a href="ViewComplaint.aspx?Id=<%#  Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="View FIR" data-toggle="tooltip" data-placement="top" class="btn btn-secondary btn-sm "><i class="fa fa-eye "></i></a>
                                                   
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div class="widget-content">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <th>

                                                        <tr>
                                                            <th class="text-center">S.No</th>
                                                            <th class="text-center">FIR No</th>
                                                            <th class="text-center">WSD Name</th>
                                                            <th class="text-center">WSD Branch</th>
                                                            <th class="text-center">Date of Complaint</th>

                                                            <th class="text-center">Part Type</th>
                                                            <th class="text-center">Status</th>
                                                            <th class="text-center" style="width: 50px;">Action</th>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <td colspan="7" style="background-color: white; color: black;">No Records Found</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:ListView>

                            </div>
                        </div>
                        <!-- end card-box-->
                    </div>
                    <!-- end col-->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
