﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Rane_WSD
{
    public class common
    {
       
       public string IsActive(string active)
       {
           if (active == "FIR Created")
           {
               return "<b class='text-primary'>" + active + "</b>";
           }
           else if (active == "Approved By Sales Engineer")
           {
               return "<b class='text-success'>" + active + "</b>";
           }
           else if (active == "Rejected By Sales Engineer")
           {
               return "<b class='text-danger'>" + active + "</b>";
           }
           else if (active == "Allocated to ASC")
           {
               return "<b class='text-secondary'>" + active + "</b>";
           }
           else if (active == "Material Dispatched to ASC")
           {
               return "<b class='text-warning'>" + active + "</b>";
           }
           else if (active == "Material Received From WSD/ASC Observation")
           {
               return "<b class='text-info'>" + active + "</b>";
           }
           else if (active == "Approved ASC FeedBack")
           {
               return "<b class='text-success'>" + active + "</b>";
           }
           else if (active == "Rejected ASC FeedBack")
           {
               return "<b class='text-danger'>" + active + "</b>";
           }
           else if (active == "Material Dispatched to WSD")
           {
               return "<b class='text-dark'>" + active + "</b>";
           }
           else if (active == "Material Received From ASC")
           {
               return "<b class='text-primary'>" + active + "</b>";
           }
           else if (active == "Material Dispatched to Warehouse")
           {
               return "<b class='text-info'>" + active + "</b>";
           }
           else if (active == "Material Received From WSD by Warehouse")
           {
               return "<b class='text-dark'>" + active + "</b>";
           }
           else if (active == "Credit Note Updated")
           {
               return "<b class='text-danger'>" + active + "</b>";
           }
           else if (active == "FIR Closed")
           {
               return "<b class='text-success'>" + active + "</b>";
           }
           else if (active == "Active")
           {
               return "<b class='text-success'>" + active + "</b>";
           }
           else if (active == "Rejected FIR Closed")
           {
               return "<b class='text-danger'>" + active + "</b>";
           }
           else if (active == "Material Sent Back To Retailer")
           {
               return "<b class='text-info'>" + active + "</b>";
           }
           else if (active == "Rejected FIR")
           {
               return "<b class='text-danger'>" + active + "</b>";
           }
           else if (active == "Admin Approved")
           {
               return "<b class='text-success'>" + active + "</b>";
           }
           else
           {
               return active;
           }
       }

    }
}